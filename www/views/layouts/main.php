<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\LtAppAsset;
use app\models\SearchForm;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ActiveField;
use app\models\Categories;

AppAsset::register($this);
LtAppAsset::register($this);
?>

<!DOCTYPE html>
<?php $this->beginPage() ?>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Comfortaa|Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <?php $this->head() ?>
</head><!--/head-->
<body>
<?php $this->beginBody() ?>
<header>
<section class="main-menu">
    <div class="container">
        <div class="row main-menu__row">
            <div class="col-3 main-menu__item text-center">

                <a href="<?= Url::to(['main/index'])?>" class="a">Главная</a>

            </div>
            <div class="col-3  main-menu__item text-center">

                <a href="" class="a">Доставка</a>

            </div>
            <div class="col-3  main-menu__item text-center">

                <a href="" class="a">Контакты</a>

            </div>
            <div class="col-3 main-menu__item no-border text-center">

                <a href="" class="a">О нас</a>

            </div>
        </div>
    </div>
</section>
<section class="contacts-section">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <a href="<?= Url::to(['main/index'])?>" class="a"><?= Html::img('@web/img/logo_dexy.png', ['class'=>'logo', 'alt'=>'logo']) ?></a>
            </div>
            <div class="col-4"></div>
            <div class="col-2 text-right">
                <i class="fas fa-shopping-basket cursor-pointer basket-icon basket__icon"></i>
            </div>
            <div class="col-2 basket-icon__info basket__icon  cursor-pointer">
                <h4>Корзина:</h4>
                <div class="basket-icon__numbers basket__icon cursor-pointer">
                    <p><?= (isset($_SESSION['basket.sum'])) ? $_SESSION['basket.sum']: 0 ?> ₽</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="search-section">
    <div class="container">
         <div class="row">
             <div class="col-2"></div>
             <div class="col-8">
                 <?php
                 $model = new SearchForm();
                 ?>
                 <?php $form = ActiveForm::begin([
                     'method'=>'get',
                     'action' => \yii\helpers\Url::to(['search/title']),
                     'options' => [
                         'class' => 'search'
                     ]
                 ]) ?>
                 <?= $form->field($model,'search')->label('')->textInput(['placeholder'=>'Поиск на сайте', 'class'=>'search__text']); ?>

                 <?php ActiveForm::end() ?>
             </div>
             <div class="col-2">
             </div>
         </div>
    </div>
</section>
<section class="categories">
        <div class="container">
            <div class="row">
                <div class="col-1 cursor-pointer">

                </div>
                <div class="col-2">
                    <div class="categories__menu">
                        <p class="cursor-pointer"><i class="fas fa-bars"></i> Каталог</p>
                        <ul>
                            <?=\app\widgets\CategoryWidget::widget()?>
                        </ul>
                    </div>
                </div>

                <div class="col-3 cursor-pointer">

                </div>
                <div class="col-3 cursor-pointer">

                </div>
                <div class="col-3 cursor-pointer">

                </div>
            </div>
        </div>
</section>
</header>
<main class="main">
    <div class="container">

    <?= $content; ?>

    </div>
</main>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-4 text-left">
                    <div class="footer-menu__icon"><?= Html::img( '@web/img/book.svg',['alt'=>'','class'=>'cursor-pointer']) ?>
                    </div>
                    <div class="footer-menu__title">
                        <h3> О компании </h3>
                    </div>
                    <div class="footer-menu__items">
                        <ul class="d-flex flex-column">
                        <li><a href="" class="a">О нас</a></li>
                        <li><a href="" class="a">Адреса</a></li>
                        <li><a href="" class="a">Партнёры</a></li>
                        <li><a href="" class="a">Услуги</a></li>
                        <li><a href="" class="a">Работа</a></li>
                    </ul>
                    </div>
                </div>
            <div class="col-4 text-left">
                <div class="footer-menu__icon">
                    <?= Html::img( '@web/img/busket_2.svg',['alt'=>'','class'=>'cursor-pointer']) ?>
                </div>
                <div class="footer-menu__title">
                    <h3>О интернет магазине </h3>
                </div>
                <div class="footer-menu__items">
                    <ul class="d-flex flex-column ">
                    <li><a href="" class="a">Доставка и оплата</a></li>
                    <li><a href="" class="a">Акции</a></li>
                    <li><a href="" class="a">Товары</a></li>
                    <li><a href="" class="a">Политика конфиденциальности</a></li>
                    <li><a href="" class="a">Правила</a></li>
                </ul>
                </div>
            </div>

            <div class="col-4 text-left">
               <div class="footer-menu__icon"> <?= Html::img( '@web/img/envelope.svg',['alt'=>'','class'=>'cursor-pointer']) ?>
               </div>
                <div class="footer-menu__title"><h3>Подписка на новости</h3>
                </div>
                <form class="subscription" action="">
                    <label for="email">Быть в курсе событий:</label>
                    <br>
                    <input type="email" id="email" placeholder="E-mail" class="subscription__email">
                    <input type="submit" value="Подписка" class="subscription__submit cursor-pointer">
                </form>
                <div class="social">
                    <p>Мы в соцсетях:</p>
                    <ul class="social__icons d-flex">
                        <li><a href=""><i class="fab fa-vk"></i></a> </li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php
\yii\bootstrap\Modal::begin([
        'header' => '<h2>Корзина</h2>',
        'id' => 'basket-modal',
        'size'=>'modal-lg',
        'footer'=> '<button type="button" class="btn btn-default" data-dismiss="modal">Продолжить покупки</button> <a type="button" class="btn btn-success" href='.Url::to(['basket/view']).'> Оформить заказ</a>
  <button type="button" class="btn btn-danger clean-basket-btn">Очистить корзину</button>  ',
]);
\yii\bootstrap\Modal::end();

?>


<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
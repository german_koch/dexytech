<?php

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $search;


?>

<?php if(!empty($products)): ?>

    <?php $i = 0 ?>

    <?php foreach ($products as $product):?>

        <?php if($i%4 === 0):?>
            <div class="row">
        <?php endif; ?>

        <div class="col-3 text-center">
        <a class="a" href="<?= Url::to(['product/view', 'id' => $product['code']])?>">
            <div class="product_mini cursor-pointer block-shadow">

                <div class="product_mini__buy text-center ">
                    <h4 class="product_mini__info__title"><?= $product['title'] ?></h4>
                    <p class="product_mini__price"><?= $product['price']?> ₽</p>
                    <p class="product_mini__descrip"><?= $product['description']?></p>
                    <object><a data-id="<?= $product['code']?>" class="product_mini__but btn add-to-basket" href="<?=Url::to(['basket/add','id'=>$product['code']]) ?>">В корзину <i class="fas fa-shopping-basket"></i></a></object>
                </div>

                <?= Html::img('@web/img/goods/'.$product['picture'],['class'=>'product_mini__picture']) ?>
                 <div class="product_mini__info text-left">
                    <h4 class="product_mini__title"><?= $product['title'] ?></h4>
                     <?php if($product['in_stock']===1): ?>
                        <p class="product_mini__inStock"><i class="fas fa-check green_fas"></i> Есть в наличии</p>
                     <?php else: ?>
                        <p class="product_mini__inStock"><i class="fas fa-times red_fas"></i> Нет в наличии</p>
                     <?php endif; ?>
                    <p class="product_mini__price"><?= $product['price']?> ₽</p>
                 </div>
            </div>
        </a>
        </div>

        <?php $i += 1; ?>

        <?php if($i%4 === 0):?>
            </div>
        <?php endif; ?>



    <?php endforeach; ?>

    <?php if($i%4 !== 0):?>
        </div>
    <?php endif; ?>
    <div style="margin-bottom: 20px" class="text-center">
    <?php
    echo \yii\widgets\LinkPager::widget([
        'pagination'=>$pages,
    ])
    ?>
    </div>

<?php else:?>
    <div class="empty_search text-center">
        <h2>Таких товаров пока нет :(</h2>
        <p>Возможно, вы ввели некорректный запрос. Если вы не смогли найти товар, в котором  нуждаетесь - обратитесь в администрацию магазина.</p>
    </div>
<?php endif; ?>





<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    $this->title = 'Корзина';
?>

<?php  if (!empty($session['basket'])): ?>
<div class="row basket-view">
    <div class="col-12">
        <div class="basket-view__check">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Наименование</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($session['basket'] as $id => $item):?>
                        <tr>
                            <td><?= \yii\helpers\Html::img('@web/img/goods/'. $item["picture"],['height'=>50,'alt'=>$item['title']])?></td>
                            <td><a href="<?=Url::to(['product/view','id'=>$id]) ?>"><?= $item['title'] ?></a></td>
                            <td><?= $item['quantity'] ?></td>
                            <td><?= $item['price'] ?></td>
                            <td><?= $item['price']*$item['quantity']?></td>
                        </tr>
                    <?php endforeach;?>
                    <tr>
                        <td colspan="4">Итого:</td>
                        <td><?= $session['basket.quantity'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">На сумму:</td>
                        <td><?= $session['basket.sum'] ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $form = ActiveForm::begin([
                'options' => [
                        'class' => 'order_form'
                ]
        ])?>
<div class="row">
    <div class="col-5">
        <?= $form->field($order,'name')?>
        <?= $form->field($order,'email')?>
        <?= $form->field($order,'phone')?>
        <?= $form->field($order,'address')?>
    </div>
    <div class="col-2">

    </div>
    <div class="col-5">
        <?= $form->field($order,'delivery')->dropDownList([
            1=>'Курьером до дома',
            0=>'Из пункта самовывоза',
        ])?>
        <?= $form->field($order,'point_address')->dropDownList([
            'Адрес'=>'Адрес',
        ])?>
        <?= $form->field($order,'payment')->dropDownList([
            1=>'Банковской картой',
            0=>'В пункте самовывоза или курьеру',
        ])?>
        <?= $form->field($order,'extra')->textarea(['rows'=>'5'])?>
    </div>
</div>
<div class="row">
    <div class="col-12 text-center">
        <?= Html::submitButton('Заказать',['class' => 'btn-order'])?>
    </div>
</div>
        <?php ActiveForm::end()?>
<?php else: ?>

    <div class="text-center" style="margin-top: 80px">
        <h3 style="color:#b13200">Корзина пуста</h3>
        <p style="margin-top: 30px">Для того, чтобы добавить какой-либо товар в корзину, наведите на него мышкой и нажмите на <span style="color: #468c6e">зелёную</span> кнопку с надписью "В корзину". Приятных покупок.</p>
    </div>
<?php endif; ?>
<?php ?>
<?php  if (!empty($session['basket'])): ?>

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['basket'] as $id => $item):?>
                    <tr>
                        <td><?= \yii\helpers\Html::img('@web/img/goods/'. $item["picture"],['height'=>50,'alt'=>$item['title']])?></td>
                        <td><?= $item['title'] ?></td>
                        <td><?= $item['quantity'] ?></td>
                        <td><?= $item['price'] ?> ₽</td>
                        <td><span class="glyphicon glyphicon-remove text-danger del-item cursor-pointer" data-id="<?= $id ?>" aria-hidden="true"></span></td>
                    </tr>
                <?php endforeach;?>
                    <tr>
                        <td colspan="4">Итого:</td>
                        <td><?= $session['basket.quantity'] ?> товаров </td>
                    </tr>
                    <tr>
                        <td colspan="4">На сумму:</td>
                        <td><?= $session['basket.sum'] ?> ₽</td>
                    </tr>
            </tbody>
        </table>
    </div>

    <?php else: ?>

    <div class="text-center">
        <h3 style="color:#b13200">Корзина пуста</h3>
        <p style="margin-top: 30px">Для того, чтобы добавить какой-либо товар в корзину, наведите на него мышкой и нажмите на <span style="color: #468c6e">зелёную</span> кнопку с надписью "В корзину". Приятных покупок.</p>
    </div>
    <?php endif; ?>
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 03/03/2019
 * Time: 20:25
 */?>
<div class="success-pay text-center">
    <h2>Мы приступили к исполнению заказа.</h2>
    <p>Если у вас остались вопросы, свяжитесь с администрацией сайта. Мы всегда будем рады вам помочь.</p>
</div>
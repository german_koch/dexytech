<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 03/03/2019
 * Time: 20:25
 */?>
<div class="error-pay text-center">
    <h2>Произошла ошибка :(</h2>
    <p>Попробуйте выполнить заказ снова черех меню корзины, или обратитесь к администрации сайта.</p>
</div>
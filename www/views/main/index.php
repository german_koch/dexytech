<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="row">
    <div class="col-12 text-center index_title">
        <h2>Популярное</h2>
    </div>
    <div class="col-12">
        <?php if(!empty($products_popular)): ?>

        <?php $i = 0 ?>

        <?php foreach ($products_popular as $product):?>

            <?php if($i%4 === 0):?>
                <div class="row">
            <?php endif; ?>

            <div class="col-3 text-center">
                <a class="a" href="<?= Url::to(['product/view', 'id' => $product['code']])?>">
                    <div class="product_index cursor-pointer block-shadow">
                        <h3 class="product_index__title"><?= $product['title']?></h3>
                        <?= Html::img('@web/img/goods/'.$product['picture'],['class'=>'product_index__picture']) ?>
                        <p class="product_index__price"><?= $product['price']?> ₽</p>
                    </div>
                </a>
            </div>

            <?php $i += 1; ?>

            <?php if($i%4 === 0):?>
                </div>
            <?php endif; ?>



        <?php endforeach; ?>

        <?php if($i%4 !== 0):?>
            </div>
        <?php endif; ?>

        <?php else:?>
            <p></p>
        <?php endif; ?>
    </div>
</div>


<div class="row">
    <div class="col-12 text-center index_title">
        <h2>Акции</h2>
    </div>
    <div class="col-12">
        <?php if(!empty($products_sale)): ?>

            <?php $i = 0 ?>

        <?php foreach ($products_sale as $product):?>

            <?php if($i%4 === 0):?>
                <div class="row">
            <?php endif; ?>

            <div class="col-3 text-center">
                <a class="a" href="<?= Url::to(['product/view', 'id' => $product['code']])?>">
                    <div class="product_index cursor-pointer block-shadow">
                        <h3 class="product_index__title"><?= $product['title']?></h3>
                        <?= Html::img('@web/img/goods/'.$product['picture'],['class'=>'product_index__picture']) ?>
                        <p class="product_index__price"><?= $product['price']?> ₽</p>
                    </div>
                </a>
            </div>

            <?php $i += 1; ?>

            <?php if($i%4 === 0):?>
                </div>
            <?php endif; ?>


        <?php endforeach; ?>

        <?php if($i%4 !== 0):?>
            </div>
        <?php endif; ?>
            <?php else:?>
                <p></p>
            <?php endif; ?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $product['title'];

?>

<div class="product_card">
    <div class="row">
        <div class="col-5">
            <?= Html::img('@web/img/goods/'.$product['picture'],['class'=>'product_card__picture']) ?>
        </div>
        <div class="col-7">
            <div class="product_card__title">
                <h3> <?= $product['title']?> </h3>
            </div>

            <div class="product_card__info">
                <div class="row">
                    <div class="col-6">
                        <h3 class="active cursor-pointer product_card__desr">Описание</h3>
                    </div>
                    <div class="col-6">
                        <h3 class="cursor-pointer product_card__feat">Характеристики</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product_card__description">
                            <?=$product['description'] ?>
                        </div>
                        <div class="product_card__features">
                            <?=$product['features'] ?>
                        </div>
                    </div>
                </div>
            </div>
                    <div class="product_card__price">
                        <p>Цена: <?= $product['price']?> ₽</p>
                    </div>
                    <div class="product_card__buy">
                        <input type="text" class="product_card__quantity" id="qty" value="1">

                        <a href=" " class="btn add-to-basket"  data-id="<?=$product['code']?>"> В корзину </a>
                    </div>
            </div>
        </div>
    </div>
</div>

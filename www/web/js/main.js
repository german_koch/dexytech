$(document).ready(function(){

    //Category

    $('.categories__menu').hover(
        function () {
            $('.categories__menu>ul').css('display','block');
        } , function () {
            $('.categories__menu>ul').css('display','none');
        }
    );

    $('.product_card__desr').click(function () {
        $('.product_card__desr').addClass('active');
        $('.product_card__feat').removeClass('active');

        $('.product_card__description').css('display','block');
        $('.product_card__features').css('display','none');
    });

    $('.product_card__feat').click(function () {
        $('.product_card__feat').addClass('active');
        $('.product_card__desr').removeClass('active');
        $('.product_card__features').css('display','block');
        $('.product_card__description').css('display','none');
    });

    $('.main').on('mouseover','.product_mini',function () {
        $(this).find('.product_mini__buy').css('opacity',1);
    });
    $('.main').on('mouseout','.product_mini',function () {
        $(this).find('.product_mini__buy').css('opacity',0);
    });

    //Basket

    function showBasket(basket) {
        $('#basket-modal .modal-body').html(basket);
        $('#basket-modal').modal();
        $.ajax({
            url: '/basket/refresh',
            type: 'POST',
            success: function (res) {
                $('.basket-icon__numbers').html(res);
            },

            error: function(){
                alert('hey');
            }
         });
    }
    
    $('.basket__icon').on('click',function () {
        $.ajax({
            url:'/basket/show',
            type: 'GET',
            success: function (res) {
                if(!res){alert('Ошибка1');}
                showBasket(res);
            },
            error: function () {
                alert('error');
            }
        });
    });

    $('.clean-basket-btn').on('click',function clearBasket(){
        $.ajax({
            url:'/basket/clear',
            type: 'GET',
            success: function (res) {
                showBasket(res);
            },
            error: function () {
                alert('error');
            }
        });
    });
    
    $('#basket-modal .modal-body').on('click', '.del-item' ,function () {
        var id = $(this).data('id');
        $.ajax({
            url:'/basket/del-item',
            data:{id:id},
            type: 'GET',
            success: function (res) {
                if(!res){alert('Ошибка2');}
                showBasket(res);
            },
            error: function () {
                alert('error');
            }
        });

    });

    $('.add-to-basket').on('click',function(event){
        event.preventDefault();
        var id = $(this).data('id');
        var quantity = $('#qty').val();
        $.ajax({
            url:'/basket/add',
            data:{id:id,  quantity: quantity},
            type: 'GET',
            success: function (res) {
                if(!res){alert('Ошибка3');}
                showBasket(res);
            },
            error: function () {
                alert('error');
            }
        });
    });
});
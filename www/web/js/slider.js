function slide($slider,$slider_left,$slider_right,$product_block) {

    $slider.on('mouseover', function () {

            $(this).children('.catalog-slider__left').css({
                'opacity': '1',
            });

            $(this).children('.catalog-slider__right').css({
                'opacity': '1',
            });
        }
    );


    $slider.on('mouseout',function () {
        $(this).children('.catalog-slider__left').css({
            'opacity': '0'
        });

        $(this).children('.catalog-slider__right').css({
            'opacity': '0'
        });
    });

     $slider_left.click(function() {
        if ($(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').position().left <= -1 && !$(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').is(':animated')) {
            $(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').animate({
                left: '+=400px'
            }, 500);
        }
    });


    $slider_right.click(function() {

        var width = $(this).parent().width();
        var width_of_products = $product_block.width();
        var num_of_products = $(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').children('.product-block').length;

        if ((-$(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').position().left + width - 200) <= (num_of_products * width_of_products) && !$(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').is(':animated')) {
            $(this).siblings('.catalog-slider__content').children('.catalog-slider__content__in').animate({
                left: '-=400px'
            }, 500);
        }
    });
}


$(document).ready(
    function(){
        $slider =$('.catalog-slider');
        $slider_left=$('.catalog-slider__left');
        $slider_right=$('.catalog-slider__right');
        $product_block=$('.product-block');

        slide($slider,$slider_left,$slider_right,$product_block);
    }
);
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 18/02/2019
 * Time: 20:13
 */

namespace app\controllers;


use app\models\Products;
use yii\web\Controller;

class ProductController extends Controller
{

    public function actionView($id){
        $product = Products::findOne($id);
        return $this->render('view',compact('product'));
    }

}
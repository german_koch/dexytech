<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 09/02/2019
 * Time: 07:18
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Products;

class MainController extends Controller
{
    public function actionIndex()
    {
        $this->view->title = 'Dexytech';
        $products_sale = Products::find()->where(['sale' => 1])->limit(8)->all();
        $products_popular = Products::find()->where(['popular' => 1])->limit(8)->all();
       return $this->render('index',compact('products_sale','products_popular'));
    }
}
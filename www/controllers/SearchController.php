<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 20:03
 */

namespace app\controllers;

use app\models\Categories;
use yii\data\Pagination;
use yii\web\Controller;
use app\models\Products;
use Yii;

class SearchController extends Controller
{
    public function actionCategory($id){
        $search = Categories::findOne($id)['category'];
        $query = Products::find()->where(['cat_num' => $id]);
        $pages = new Pagination(['totalCount'=>$query->count(), 'pageSize' => 8]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        return  $this->render('category',compact('products','pages','search'));
    }

    public function actionTitle()
    {
        $search = trim(Yii::$app->request->get('SearchForm')['search']);

        if (!empty($search)) {
            $query1 = Products::find()->where(['like', 'title', $search]);
            $pages = new Pagination(['totalCount' => $query1->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
            $products = $query1->offset($pages->offset)->limit($pages->limit)->all();

            if (empty($products)) {
                $query2 = Products::find()->where(['cat_num' => Categories::find()->where(['like', 'category', $search])->one()['cat_num']]);
                $pages = new Pagination(['totalCount' => $query2->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
                $products = $query2->offset($pages->offset)->limit($pages->limit)->all();
                return $this->render('category', compact('products', 'pages', 'search'));
            }
            return $this->render('category', compact('products', 'pages', 'search'));
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

}
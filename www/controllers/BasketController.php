<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 20/02/2019
 * Time: 23:27
 */

namespace app\controllers;


use yii\web\Controller;
use app\models\Products;
use app\models\Basket;
use app\models\OrderItems;
use app\models\Orders;
use app\components\Pay2MeApi;
use Yii;

class BasketController extends Controller
{
    public function actionAdd(){
        $id = Yii::$app->request->get('id');
        $quantity =  abs((int)Yii::$app->request->get('quantity'));
        $quantity = !$quantity ? 1 : $quantity ;
        $product = Products::findOne($id);
        if (empty($product)){
            return false;
        }
        $session = Yii::$app->session;
        $session->open();
        $basket = new Basket();
        $basket->addToBasket($product,$quantity);
        $this->layout = false;
        if(!Yii::$app->request->isAjax){
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('basket-modal',compact('session','quantity'));
    }

    public function actionClear(){
        $session = Yii::$app->session;
        $session->open();
        $session->remove('basket');
        $session->remove('basket.quantity');
        $session->remove('basket.sum');
        $this->layout = false;
        return $this->render('basket-modal',compact('session'));
    }

    public function actionDelItem(){
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $basket = new Basket();
        $basket->recalc($id);
        $this->layout = false;
        return $this->render('basket-modal', compact('session'));

    }

    public function actionDelItemView(){
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $basket = new Basket();
        $basket->recalc($id);
        $this->layout = false;
        return $this->render('view', compact('session'));

    }

    public function actionShow(){
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('basket-modal', compact('session'));
    }

    public function actionView(){
        $session = Yii::$app->session;
        $session->open();

        $order = new Orders();
        if($order->load(Yii::$app->request->post())) {
            $order->qty = $session['basket.quantity'];
            $order->sum = $session['basket.sum'];
            if (!$order->payment){
                $order->status = "cash";
                $order->order_desc = 'Наличные';
                if($order->save()){
                    $this->saveOrderItems($session['basket'], $order->order_code);
                    $session->removeAll();
                    return $this->render('success-pay');
                }
            }
            if ($order->payment) {
                return $this->payme($order);
            }
        }
        return $this->render('view',compact('session','order'));
    }

    protected function payme($order){
        $session = Yii::$app->session;
        $session->open();
        $pay2me = new Pay2MeApi();
        $order->order_desc= "Оплата заказа";
        if($order->save()) {
            $result = $pay2me->dealCreate($order->order_code, $order->order_desc, $order->sum, 0);
            $order->signature = $result->signature;
            $order->status = $result->status;
            $order->save();
            $this->saveOrderItems($session['basket'], $order->order_code);
            return $this->redirect($result->redirect);
        }
        return $this->render('error-pay');
    }

    public function actionPayResult(){
        $result = $_POST;
        $_SESSION['order_id'] = $result['order_id'];
        $_SESSION['pay_status'] = $result['status'];
    }

    public function actionSuccessPay(){
        $session = Yii::$app->session;
        $session->open();
        $order = Orders::findOne($_SESSION['order_id']);
        $order->status = $_SESSION['pay_status'];
        $session->removeAll();
        return $this->render('success-pay');
    }


    public function actionErrorPay(){
        return $this->render('error-pay');
    }

    protected function saveOrderItems($items,$order_code){
        foreach ($items as $id=>$item){
            $order_items = new OrderItems();
            $order_items->order_code = $order_code;
            $order_items->prod_code = $id;
            $order_items->prod_title = $item['title'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['quantity'];
            $order_items->sum_item = $item['price']*$item['quantity'];
            $order_items->save();
        }
    }




    public function actionRefresh(){
        $session = ['sum'=> (isset($_SESSION['basket.sum'])) ? $_SESSION['basket.sum']: 0];

        return '<p>'.$session['sum']. ' ₽ </p>';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 09/02/2019
 * Time: 09:19
 */

namespace app\assets;


use yii\web\AssetBundle;

class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/slider.css',
    ];

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'grozzzny\depends\bootstrap4\Bootstrap4Asset',
        'grozzzny\depends\bootstrap4\Bootstrap4PluginAsset',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 20/02/2019
 * Time: 23:33
 */

namespace app\models;
use yii\db\ActiveRecord;

class Basket extends ActiveRecord
{

    public function addToBasket($product, $quantity = 1){
        if(isset($_SESSION['basket'][$product['code']])){
            $_SESSION['basket'][$product['code']]['quantity'] += $quantity;
        } else {
            $_SESSION['basket'][$product['code']] = [
                'quantity' => $quantity,
                'title' => $product['title'],
                'price' => $product['price'],
                'picture' => $product['picture'],
            ];
        }
        $_SESSION['basket.quantity'] = isset($_SESSION['basket.quantity']) ? $_SESSION['basket.quantity'] + $quantity : $quantity;
        $_SESSION['basket.sum'] = isset($_SESSION['basket.sum']) ? $_SESSION['basket.sum'] + $quantity*$product['price'] : $quantity*$product['price'];
    }

    public function recalc($id){
        if(!isset($_SESSION['basket'][$id])) return false;
        $quantityMinus = $_SESSION['basket'][$id]['quantity'];
        $sumMinus = $_SESSION['basket'][$id]['quantity']*$_SESSION['basket'][$id]['price'];
        $_SESSION['basket.quantity'] -= $quantityMinus;
        $_SESSION['basket.sum'] -= $sumMinus;
        unset($_SESSION['basket'][$id]);
    }

}
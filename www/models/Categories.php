<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 19:07
 */

namespace app\models;
use yii\db\ActiveRecord;

class Categories extends ActiveRecord
{
    public function getProducts(){
        return $this->hasMany(Products::className(),['cat_num'=>'cat_num']);
    }
}
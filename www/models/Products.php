<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 19:06
 */

namespace app\models;
use yii\db\ActiveRecord;


class Products extends ActiveRecord
{

    public function getCategories(){
        return $this->hasOne(Categories::className(),['cat_num'=>'cat_num']);
    }

    public function getProducts(){
        return $this->hasMany(OrderItems::className(),['prod_code'=>'code']);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 19:06
 */

namespace app\models;
use yii\db\ActiveRecord;

class OrderItems extends ActiveRecord
{
    public function getOrders(){
        return $this->hasOne(Orders::className(),['order_code'=>'order_code']);
    }

    public function getProducts(){
        return $this->hasOne(Products::className(),['code'=>'prod_code']);
    }

    public function rules(){
        return[
            [['prod_title'],'string','max'=>255],
            [['sum_item','price'],'number'],
            [['order_code','prod_code','qty_item'],'integer']
        ];

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 19:06
 */

namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Orders extends ActiveRecord
{
    public function getOrderItems(){
        return $this->hasMany(OrderItems::className(),['order_code'=>'order_code']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],

                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules(){
        return[
            [['name','email','address','phone','delivery','payment'],'required'],
            [['email'],'email'],
            [['qty'],'integer'],
            [['sum'],'number'],
            [['name','email','address'],'string','max'=>255],
            [['extra'],'string','max'=>1000],
        ];

    }

    public function attributeLabels()
    {
        return [
            'name'=>'Имя:',
            'email'=>'email:',
            'phone'=>'Контактный телефон:',
            'address'=>'Ваш адрес:',
            'delivery'=>'Тип доставки:',
            'payment'=>'Способ оплаты:',
            'extra'=>'Дополнительно:',
            'point_address'=>'Пункт самовывоза:'
        ];
    }
}
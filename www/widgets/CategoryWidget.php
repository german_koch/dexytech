<?php
/**
 * Created by PhpStorm.
 * User: German
 * Date: 14/02/2019
 * Time: 23:52
 */

namespace app\widgets;


use yii\base\Widget;
use app\models\Categories;

class CategoryWidget extends Widget
{
    public function run()
    {
        $result = '';
        $categories = Categories::find()->asArray()->all();
        foreach ($categories as $cat){
            $result.= "<li><a class='a' href =". \yii\helpers\Url::to(['search/category', 'id'=>$cat['cat_num']]).">" . $cat['category'] . " </a></li> ";
        }

        return $result;
    }
}
